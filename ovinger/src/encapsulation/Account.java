package encapsulation;

public class Account {
	double balance;
	double interestRate;
	
	Account(){
		
	}
	
	Account(double balance, double interestRate){
		deposit(balance);
		setInterestRate(interestRate);
	}
	
	double getBalance() {
		return balance;
	}
	double getInterestRate() {
		return interestRate;
	}
	void setInterestRate(Double interestRate){
		if (interestRate >= 0) {
			this.interestRate = interestRate;
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	void setInterestRate(int interestRate){
		double ir = interestRate;
		setInterestRate(ir);
	}
	void deposit(double amount) {
		if(amount >= 0){
			balance += amount;
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	void withdraw(double amount) {
		if(amount >= 0 & amount <= balance){
			balance -= amount;
		}else{
			throw new IllegalArgumentException();
		}
	}
	
	void addInterest() {
		balance += balance * interestRate/100;
	}
	@Override
	public String toString() {
		return(String.format("balance = %d, interest rate = %d",balance, interestRate));
	}
	public static void main(String[] args) {
		Account a1 = new Account(0, 5);
		System.out.println(a1);
		a1.deposit(100);
		System.out.println(a1.getBalance());
		a1.addInterest();
		System.out.println(a1.getInterestRate());
		a1.setInterestRate(10.0);
		System.out.println(a1);
	}
}
