package encapsulation;

public class Digit{
    private int base;
    private int value;

    Digit(int base){
        if(base <= 0){
            throw new IllegalArgumentException();
        }
        else
        {    
        this.base = base;
        value = 0;
        }
    }
    public int getValue(){
        return value;
    }
    public boolean increment(){
        if(value == base - 1){
            value = 0;
            return true;
        }
        else{
            value ++;
            return false;
        }
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return Integer.toString(value);
    }
    public int getBase(){
        return base;
    }
}