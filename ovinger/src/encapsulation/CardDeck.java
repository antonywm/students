package encapsulation;

import java.util.ArrayList;

public class cardDeck{
    private ArrayList<Card> cards=new ArrayList<Card>();

    cardDeck(int n){
        if(n>13){
            n= 13;
        }
        if(n<1){
            n=1;
        }

        for(int i = 0; i<=3; i++){
            for(int t = 1; t<=n; t++){
                Card toAdd = new Card(allowedSuits.get(i), t);
                cards.add(toAdd);}}
    }

    public int getCardCount(){
        return cards.size();
    }

    public Card getCard(int n){
        if(n<0 || n>cards.size()){
            throw new IllegalArgumentException();
        }else{
            return cards.get(n);
        }
    }
    public void shuffleCard(){
        
    }
}