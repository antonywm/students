package encapsulation;

import java.util.*;

import java.lang.reflect.GenericSignatureFormatError;

public static class Card{
    private String suit;
    private int face;
//bruker konsekvent ikke modifier her sånn at lista kan brukes i cardDeck
    ArrayList<Character> allowedSuits=new ArrayList<Character>(
        Arrays.asList('S', 'H', 'D', 'C')
        ); 

    Card(char suit, int number){
        if(allowedSuits.contains(suit) && number >= 1 && number <= 13){
            suit = this.suit;
            number = this.number;
        }else{
            throw new IllegalArgumentException();
        }
    }
    public String getSuiSuit(){
        return suit;
    }
    public String getFace(){
        return face;
    }
    public String toString(){
        String toReturn = String.format("%s %d", suit, face);
    }
   
}