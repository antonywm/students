package encapsulation;

public class UpOrDownCounter{
    private int counter;
    private int end;

    UpOrDownCounter(int start, int end){
        if(start == end){
            throw new IllegalArgumentException();
        }else{
            counter = start;
            this.end = end;
        }
    }
    public int getCounter(){
        return counter;
    }
    public boolean count(){
        counter += Math.signum(end - counter);
        return counter != end;  
    }
} 