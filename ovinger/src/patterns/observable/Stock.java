package patterns.observable;

import java.util.ArrayList;
import java.util.List;

public class Stock{
    private double stockPrice;
    private String ticker;
    private List<StockListener> listenerList = new ArrayList<>();

    public Stock(String ticker, double stockPrice){
        setPrice(stockPrice);
        this.ticker = ticker;
    }

    public void setPrice(double stockPrice){
        if(stockPrice <= 0){
            throw new IllegalArgumentException();
        }else{
            for(StockListener listener : listenerList){
                listener.stockPriceChanged(this, this.stockPrice, stockPrice);
            }
            this.stockPrice = stockPrice;
        }
    }
    public String getTicker(){
        return ticker;
    }
    public double getPrice(){
        return stockPrice;
    }
    void addStockListener(StockListener listener){
        if (!listenerList.contains(listener)) {
			listenerList.add(listener);
        }
    }
    void removeStockListener(StockListener listener){
        if (listenerList.contains(listener)){
            listenerList.remove(listener);
        }
    }
}
