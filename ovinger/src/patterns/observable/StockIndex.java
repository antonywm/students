package patterns.observable;

import java.util.ArrayList;
import java.util.List;

class StockIndex implements StockListener{
    private double index;
    private List<Stock> stockList = new ArrayList<>();

    public StockIndex(String name, Stock... stock){
        for(Stock s : stock){
            stockList.add(s);
        }
        /* if(stockList.size() == 0){
            index == 0;
        } */
        index = 0;
        for(Stock s : stockList){
            index += s.getPrice();
        }
    }
    void addStock(Stock stock){
       /* if (!stockList.contains(stock)) {
            stockList.add(stock);
            index += stock.getPrice();
        } */ 
    	stockList.add(stock);
    	index += stock.getPrice();
    }
    void removeStock(Stock stock){
        /* if(stockList.contains(stock)){
            stockList.remove(stock);
            index -= stock.getPrice();
        } */ 
        stockList.remove(stock);
        index -= stock.getPrice();
    }
    double getIndex(){
        return index;
    }

	@Override
	public void stockPriceChanged(Stock stock, double oldValue, double newValue) {
		int listIndex = stockList.indexOf(stock);
		stockList.get(listIndex).setPrice(newValue);
	}
}
