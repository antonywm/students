package stateandbehavior;

import java.util.HashMap;

class Location{
    private HashMap<String, Integer> coordinates = new HashMap<String, Integer>();

    Location(){
        coordinates.put("x", 0);
        coordinates.put("y", 0);
    }
    public void up(){
        coordinates.replace("y", coordinates.get("y") - 1);
    }
    public void down(){
        coordinates.replace("y", coordinates.get("y") + 1);
    }
    public void left(){
        coordinates.replace("x", coordinates.get("x") - 1);
    }
    public void right(){
        coordinates.replace("x", coordinates.get("x") + 1);
    }
    public int getX(){
        return coordinates.get("x");
    }
    public int getY(){
        return coordinates.get("y");
    }
}