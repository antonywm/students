package stateandbehavior;

public class UpOrDownCounter{
    int counter;
    int end;

    UpOrDownCounter(int start, int end){
        if(start == end){
            throw new IllegalArgumentException();
        }else{
            counter = start;
            this.end = end;
        }
    }
    int getCounter(){
        return counter;
    }
    boolean count(){
        // if(counter < end){
        //     counter += 1;
        //     return true;
        // }else if(counter > end){
        //     counter -= 1;
        //     return true;
        // }
        // else{
        //     return false;
        // }

        counter += Math.signum(end - counter);
        return counter != end;  
    }
} 