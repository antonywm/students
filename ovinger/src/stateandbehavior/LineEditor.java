package stateandbehavior;

public class LineEditor{
    StringBuffer text = new StringBuffer();
    int insertionIndex = 0;

    boolean checkLeft(){
        return insertionIndex > 0;
    }
    boolean checkRight(){
        return insertionIndex < text.length();
    }

    void left(){
        if(checkLeft()){
            insertionIndex -= 1;
        }
    }

    void right(){
        if(checkRight()){
            insertionIndex += 1;
        }
    }
    void deleteText(){
        text.delete(0, text.length());
    }
    void insertString(String s){
        text.insert(insertionIndex, s);
        setInsertionIndex(insertionIndex + s.length());
    }
    void deleteLeft(){
        if(checkLeft()){
            text.delete(insertionIndex - 1, insertionIndex);
            left();
        }else{
            text.replace(0, 1, "");
        }
    }
    void deleteRight(){
        if(checkRight()){
            text.delete(insertionIndex, insertionIndex + 1);
        }
    }
    String getText(){
        return text.toString();
    }
    void setText(String text){
        deleteText();   
        this.text.append(text);
        
    }
    int getInsertionIndex(){
        return insertionIndex;
    }
    void setInsertionIndex(int insertionIndex){
        this.insertionIndex = insertionIndex;
    }
    @Override
    public String toString() {
        StringBuffer s = text;
        s.insert(insertionIndex, "|");
        return s.toString();
        // insertString("|");
        // return getText();
    }
    public static void main(String[] args) {
        LineEditor l1 = new LineEditor();
        l1.setText("Java");
        l1.setInsertionIndex(3);
        l1.insertString("Java");
        System.out.println(l1.getText());

    //     System.out.println(l1.getText());
    //     l1.deleteRight();
    //     System.out.println(l1.getText());
    // }
    }
}