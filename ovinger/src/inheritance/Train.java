package inheritance;

import java.util.ArrayList;

public class Train {
	private ArrayList<TrainCar> trainCars = new ArrayList<>();
	
	void addTrainCar(TrainCar trainCar){
		trainCars.add(trainCar);
	}
	
	boolean contains(TrainCar trainCar) {
		return trainCars.contains(trainCar);
	}
	public int getTotalWeight() {
		int weight = 0;
		for(TrainCar t : trainCars) {
			weight += t.getTotalWeight();
		}
		return weight;
	}
	int getPassengerCount() {
		int count = 0;
		for(TrainCar t : trainCars) {
			if(t instanceof PassengerCar) {
				count += ((PassengerCar) t).getPassengerCount();
			}
		}
		return count;
	}
	int getCargoWeight() {
		int weight = 0;
		for(TrainCar t: trainCars) {
			if(t instanceof CargoCar) {
				weight += ((CargoCar) t).getCargoWeight();
			}
		}
		return weight;
	}
	
	@Override
	public String toString() {
		String train = "";
		for(TrainCar t : trainCars) {
			train += t.toString();
		}
		return train;
	}
}
