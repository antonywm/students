package inheritance;

public class CargoCar extends TrainCar {
	private int cargoWeight;
	
	
	public CargoCar(int deadWeight, int cargoWeight) {
		super(deadWeight);
		this.cargoWeight = cargoWeight;
	}

	public int getCargoWeight() {
		return cargoWeight;
	}

	public void setCargoWeight(int cargoWeight) {
		this.cargoWeight = cargoWeight;
	}
	
	@Override
	public int getTotalWeight() {
		// TODO Auto-generated method stub
		return cargoWeight + deadWeight;
	}
	@Override
	public String toString() {
		return String.format("CargoCar|total weight: %d| cargo weight: %d \n", getTotalWeight(), cargoWeight);
	}
}
