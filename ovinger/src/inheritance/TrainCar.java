package inheritance; 

class TrainCar{
    int deadWeight;

    TrainCar(int deadWeight){
        setDeadWeight(deadWeight);
    }
    public int getTotalWeight(){
        return getDeadWeight();
    }
    public void setDeadWeight(int deadWeight){
        this.deadWeight = deadWeight;
    }
    int getDeadWeight(){
        return deadWeight;
    }
}