package inheritance;

public class PassengerCar extends TrainCar {
	
	private int passengerCount;
	
	PassengerCar(int deadWeight, int passengerCount){
		super(deadWeight);
		setPassengerCount(passengerCount);
	}
	public int getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(int passengerCount) {
		this.passengerCount = passengerCount;
	}
	@Override
	public int getTotalWeight() {
		return deadWeight + passengerCount * 80;
	}
	@Override
	public String toString() {
		return String.format("Passenger car| weight: %d| passenger count: %d", getTotalWeight(), passengerCount);
	}
}
