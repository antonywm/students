package inheritance;

public class DebitAccount extends AbstractAccount {

	@Override
	void internalWithdraw(double amount) {
		if(amount < 0) {
			throw new IllegalArgumentException();
		}
		if(amount > balance) {
			throw new IllegalStateException();
		}
		balance -= amount;
	}
	
}
