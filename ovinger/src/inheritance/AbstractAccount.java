package inheritance;

public abstract class AbstractAccount {
	protected double balance = 0;
	
	void deposit(double amount) {
		if(amount < 0) {
			throw new IllegalArgumentException();
		}
		balance += amount;
	}
	void withdraw(double amount) {
		if(amount< 0) {
			throw new IllegalArgumentException();
		}
		internalWithdraw(amount);
	}
	abstract void internalWithdraw(double amount);
	
	public double getBalance() {
		return balance;
	}
}
