package inheritance;

public class SavingsAccount2 extends AbstractAccount{

	private int withdrawals;
	private double fee;
	@Override
	void internalWithdraw(double amount) {
		if(withdrawals <= 0) {
			amount += fee;
		}
		if(amount > balance) {
			throw new IllegalStateException();
		}
		balance -= amount;
		withdrawals -= 1;
	}

	SavingsAccount2(int withdrawals, double fee){
		this.withdrawals = withdrawals;
		this.fee = fee;
	}
}
